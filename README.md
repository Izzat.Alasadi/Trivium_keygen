 
# Trivium Cipher
    •	the 80-bit key is loaded into the first 80 bits of the 84-degree LFSR (the “first” bits here refers to the left-most 80 bits); 
    •	the 80-bit initialization vector is loaded into the first 80 bits of the 93- degree LFSR; 
    •	the right-most 3 bits of the 111-degree LFSR are set to 1; 
    •	all remaining bits are set to 0. 
    •	Recall that the cipher performs 1152 “warm-up” steps before beginning to pro- duce output. 
    Use your implementation with key K = (1,0,1,0,...,1,0) (80 alternating 
    ones and zeros) and IV = (0, 1, . . . , 0, 1) (80 alternating zeros and ones) to generate 50 bits of keystream. 

## Notation	discreption
    (𝑠𝑚,...,𝑠𝑛)	the internal state of (𝑚−𝑛+1) bits
    𝑠𝑖	the 𝑖𝑡h state or bit in the shift registers
    𝑧𝑡	the bit of keystream generated 
    +	addition over 𝐺𝐹(2), i.e. XOR
    .	multiplication over 𝐺𝐹 (2), i.e. AND
    {𝑎, 𝑏, 𝑐}	notation of one round in Trivium by its active bits: 𝑎𝑡h bit, 𝑏𝑡h bit and 𝑐𝑡h bit
        
        

## Initialization
    Key size	80 bits
    IV size:	80 bits
    Internal state	288 bits
    Initial state	(s1, s2, …, s288)
    


## LFSRs

    LFSR (93 bits) include the IV	[s1,s2,s3,…,s80, x81,x82,..,x93]
    LFSR (84 bits) include the KEY	[k1, k2, k3,.., k80,x81, x82, x83, x84]
    LFSR (111 bits)	[x1=0,x2=0,….,x108=0,x109=1,x110=1,x11=1]

    So we have (93+84+111 bits) 288 bits internal state
        for 𝑖=0 to 𝑁 do
            𝑡1 ← 𝑠66 + 𝑠93 
            𝑡2 ← 𝑠162 +𝑠177 
            𝑡3 ← 𝑠243 +𝑠288
            𝑧𝑖 ← 𝑡1 + 𝑡2 + 𝑡3 
            𝑡1 ← 𝑡1+ 𝑠91 . 𝑠92 + 𝑠171 
            𝑡2 ← t2 + 𝑠175 . 𝑠176 +𝑠264 
            𝑡3 ← t3 + 𝑠286 . 𝑠287+𝑠69
            (𝑠1,𝑠2,...,𝑠93) ← (𝑡3,𝑠1,...,𝑠93) 
            (𝑠94,𝑠95,...,𝑠177) ← (𝑡1,𝑠94,...,𝑠176) 
            (𝑠178,𝑠179,...,𝑠288) ← (𝑡2,𝑠178,...,𝑠287) 

### output:

    KEY: 80 bits, and has bits sequence: [1, 0, 1, 0]
    MCL: 1208925819614629174706175
    IV: 80 bits and has bits sequence: [0, 1, 0, 1]
    (s1; ; : : : ; s288):
    101010101010101010101010101010101010101010101010101010101010101010101010101010100000010101010101010101010101010101010101010101010101010101010101010101010101010101010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000111
    Generated keystream (first 50bits): 01100101101100101010110011110010000111010110001001
