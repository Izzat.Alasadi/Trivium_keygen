from collections import deque
from itertools import repeat

class Trivium:
    def __init__(self, key, iv):
        
        self.state = None
        self.counter = 0
        self.key = key  
        self.iv = iv  

        # Initialize state

        # len 84
        #[k1,k2,k3...,k80,x81,x82,x83,x84]
        """the 80-bit key is loaded into the first 80 bits of the 84-degree LFSR 
        (the “first” bits here refers to the left-most 80 bits); """
        init_list = list(map(int, list(self.key)))
        init_list += list(repeat(0, 4))
        
        #len 93
        #[s1,s2,s3...,s80,..,x93]
        """•	the 80-bit initialization vector is loaded into 
        the first 80 bits of the 93- degree LFSR """
        init_list += list(map(int, list(self.iv)))
        init_list += list(repeat(0, 13))
        
        # len 111
        init_list += list(repeat(0, 108))
        init_list += list([1, 1, 1])
        self.state = deque(init_list)
        
        temp="".join([str(int) for int in init_list])
        print("(s1; ; : : : ; s288):\n{}".format(temp))
        
        # Do 4 full cycles, drop output
        for i in range(4*288):
            self._gen_keystream()

    def keystream(self):
        while self.counter < 2**64:
            self.counter += 1
            yield self._gen_keystream()
        

    
    def _gen_keystream(self):
        """this method generates triviums keystream"""

        t_1 = self.state[65] ^ self.state[92]
        t_2 = self.state[161] ^ self.state[176]
        t_3 = self.state[242] ^ self.state[287]

        z_i = t_1 ^ t_2 ^ t_3

        s_1= t_1 ^ self.state[90] & self.state[91] ^ self.state[170]
        s_2= t_2 ^ self.state[174] & self.state[175] ^ self.state[263]
        s_3= t_3 ^ self.state[285] & self.state[286] ^ self.state[68]

        self.state.rotate()        

        self.state[0] = s_3
        self.state[93] = s_1
        self.state[177] = s_2

        return z_i


def main():
    seq="11"*40
    KEY=[int(i) for i in list(seq)]
    
    print("KEY: {} bits, and has bits sequence: {}".format(len(KEY),KEY[:4])
    
    seq="00"*40
    IV = [int(i) for i in list(seq)]
    
    print("IV: {} bits and has bits sequence: {}".format(len(IV),IV[:4]))
    
    trivium = Trivium(KEY, IV)
    next_key_bit = trivium.keystream().__next__
    
    for i in range(1):
        keystream = []
        for j in range(1000):
            keystream.append(next_key_bit())
    keystream = "".join([str(int) for int in keystream])
    print("Generated keystream ({}): {}".format(len(keystream),keystream))
         
        


if __name__ == "__main__":
    main()
